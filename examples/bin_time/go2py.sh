#!/bin/sh

script_dir=$(dirname -- "$( readlink -f -- "$0"; )")


go run "$script_dir/main.go" | python3 "$script_dir/main.py"