package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func main() {
	fi, err := os.Stdin.Stat()
	if err != nil {
		log.Fatal(err)
	}
	if fi.Mode()&os.ModeNamedPipe == 0 {
		marshal()
	} else {
		unmarshal()
	}
}

func marshal() {
	// t := time.Now()
	t := time.Date(2023, 12, 31, 12, 42, 59, 987654321, time.Local)
	b, err := t.MarshalBinary()
	if err != nil {
		log.Fatal()
	}
	if _, err := os.Stdout.Write(b); err != nil {
		log.Fatal(err)
	}
}

func unmarshal() {
	b, err := io.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	var t time.Time
	if err := t.UnmarshalBinary(b); err != nil {
		log.Fatal(err)
	}
	if _, err := fmt.Fprint(os.Stdout, t.Format(time.RFC3339Nano)); err != nil {
		log.Fatal(err)
	}
}
